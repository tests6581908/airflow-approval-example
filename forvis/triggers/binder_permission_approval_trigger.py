from typing import Any
import asyncio
from airflow.models import Variable
from airflow.triggers.temporal import TimeDeltaTrigger
from datetime import datetime
from airflow.triggers.base import BaseTrigger, TriggerEvent


class BinderPermissionApprovalTrigger(BaseTrigger):
    def __init__(self, approval_request_id: int):
        super().__init__()
        self.approval_request_id = approval_request_id

    def serialize(self) -> tuple[str, dict[str, int]]:
        return ("forvis.triggers.binder_permission_approval_trigger.BinderPermissionApprovalTrigger", {"approval_request_id": self.approval_request_id})

    async def run(self):
        while str(Variable.get("approval")).lower().strip() != "true":
            await asyncio.sleep(10)
        yield TriggerEvent(self.approval_request_id)