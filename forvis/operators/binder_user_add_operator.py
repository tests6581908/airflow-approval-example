from airflow.models.baseoperator import BaseOperator


class BinderUserAddOperator(BaseOperator):
    def __init__(self, name: str, **kwargs) -> None:
        super().__init__(**kwargs)
        self.name = name

    def execute(self, context):
        message = f"Pretend I'm adding someone to a binder!"
        return message
