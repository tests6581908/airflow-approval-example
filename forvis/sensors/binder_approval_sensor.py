from datetime import timedelta

from airflow.sensors.base import BaseSensorOperator
from airflow.utils.decorators import apply_defaults
from airflow.triggers.temporal import TimeDeltaTrigger
from forvis.triggers.binder_permission_approval_trigger import BinderPermissionApprovalTrigger

class BinderApprovalSensor(BaseSensorOperator):
    @apply_defaults
    def __init__(self, filepath, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.file_path = filepath


    def execute(self, context):
        # self.defer(trigger=TimeDeltaTrigger(timedelta(hours=1)), method_name="execute_complete")
        self.defer(trigger=BinderPermissionApprovalTrigger(approval_request_id=21), method_name="execute_complete")

    def execute_complete(self, context, event=None):
        # We have no more work to do here. Mark as complete.
        return

