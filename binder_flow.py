from datetime import datetime, timedelta
from airflow import DAG
import sys
from os.path import abspath, dirname
sys.path.insert(0, dirname(dirname(abspath(__file__))))


from forvis.triggers.binder_permission_approval_trigger import BinderPermissionApprovalTrigger
from forvis.sensors.binder_approval_sensor import BinderApprovalSensor
from forvis.operators.binder_user_add_operator import BinderUserAddOperator




default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 2, 23),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

with DAG('binder_user_add_dag',
         default_args=default_args,
         schedule_interval=None) as dag:

    # wait_for_approval = BinderApprovalSensor(task_id='wait_for_approval', trigger=BinderPermissionApprovalTrigger(21), filepath="test")
    wait_for_approval = BinderApprovalSensor(task_id="wait_for_approval", filepath="test")
    add_user = BinderUserAddOperator(task_id='add_user', name='John Doe')

    wait_for_approval >> add_user